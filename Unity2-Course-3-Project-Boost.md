## Minimum viable product (MVP)

Use arrows to move a rocket in a 3d environment. Arrows rotate the rocket, and boost button (space ?) is to thrust the rocket forward.

Avoid the walls of the 3d environment or you explode and die. Land on the goal before the fuel goes to zero.

## Create a new Unity project

Once created go into Edit->Project Settings->Editor

Make sure the Visible Meta Files option is selected and the Asset Serialization is in Force Text mode.

Then save the project and the scene as Assets/Game.unity. You can then check things in (with gitignore and gitattributes of course).

## Create the basic geometry

![Project Boost first terrain launchpad](project-boost-first-terrain-launchpad.png)

Check the pivot mode. In center mode, the rotation is based on center of gravity. Pivot mode is selected by hand. You can move the inner geometry lower so that the pivot mode of the base falls higher. Then move the object up so that the geometry is back where it was.

Now if you instantiate (after you've templatized the rocket ship). If you instantiate while in "Pivot mode" the pivot point will be set on the ground (so rocket half in the ground depending on where the pivot point is). If on the other hand you instantiate (drag and drop from asset to scene) while in Center mode, then Unity will correctly make sure your rocket stands on its feet on the ground, no need to finesse the position so that there's no gap or anything. You should be good on Y axis. But may need to correct X and Z axis (for example 0/0 to center them on the launch pad).

Changing from pivot to center mode is hitting Z or the center/pivot button on top of the screen.

![Project Boost rocket prototype](project-boost-rocket-prototype.png)

## Apply Physics

The default physical behavior is simple. Select the Rocket Ship empty game object. In the inspector window click on add component. Then Physics->rigidbody. 

Give the object a weight, and then select to apply "Gravity". 

To see that it works move the object slightly above ground then hit the PLAY button. The object should fall back on the ground.

In a script attached to an object, you can grab the Rigidbody component associated to the object via GetComponent<Rigidbody>() (same syntax for any kind of component).

rigidBody.AddRelativeForce() adds a force in the object space rather than in the world space (good for thrust of a rocket who always is in the direction from tail to nose).

Get the delta time of the last frame via Time.deltaTime. 

Affect the rotation/transformation of an object directly via transform.Rotate().

## Optimal forces

Terminal velocity due to gravity : 
mass * gravity = drag * terminal velocity
 
if we want velocity to be 1m per second then we set
drag = mass * gravity / terminal velocity 



